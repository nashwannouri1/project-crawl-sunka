package com.firstapp.nashwan.finalsungka;

import android.util.Log;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by nashwan on 10/21/2015.
 */
public class Game {

    private Player player1, player2;
    private Player current, opponent;
    private int _currentShells, _opponentShells;
    private boolean firstTime;
    private ArrayList<Tray> currentPTray, opponentPTray;


    public Game(Player p1, Player p2) {

        _opponentShells = 0;
        _currentShells = 0;
        player1 = p1;
        player2 = p2;
        player1.setIsTurn(true);
        player2.setIsTurn(true);
        firstTime = true;
        currentPTray = new ArrayList<>();
        opponentPTray = new ArrayList<>();

    }

    /**
     * checks to see if move is valid
     *
     * @param v view clicked to start the move
     */
    public void runMove(View v) {

        Log.d("checks", "noview");

        //checks to see if player one clicked valid
        if (player1.ViewBelongsToPlayer(v)) {

            //initials move
            Log.d("checks", "viewBelongs");
            theMove(player1, player2);
            player1.setCanMakeTurn(false);
        }

        //checks to see if player2 clicked valid
        else if (player2.ViewBelongsToPlayer(v)) {

            //initials move
            Log.d("checks", "viewBelongs");
            theMove(player2, player1);
            player2.setCanMakeTurn(false);
        }
    }

    /**
     * the actual move is processed and trays updated
     *
     * @param i     the current move maker
     * @param other the opponent
     */
    public void theMove(final Player i, Player other) {
        Log.d("checks", "entered theMove class");
        current = i;
        opponent = other;


        Gamer run = new Gamer() {
        };
        run.run();
    }


    public class Gamer implements Runnable {

        public boolean updateTray(int index, ArrayList<Tray> current) {
            current.get(index).addShell(1);
            _currentShells--;
            if (_currentShells == 0) {
                return true;
            }
            return false;
        }

        public boolean updateTrayp2(int index, ArrayList<Tray> current) {
            current.get(index).addShell(1);
            _opponentShells--;
            if (_opponentShells == 0) {
                return true;
            }
            return false;
        }

        public void normalUpdate() {
            Log.d("ket","entering normal update");
            Log.d("checks", _currentShells + "");
            Tray clicked = current.getSelectedTray();
            clicked.removeShell();

            for (int i = currentPTray.indexOf(clicked) + 1; i < 7; ++i) {
                if (_currentShells == 1 && currentPTray.get(i).getShells() == 0) {
                    currentPTray.get(i).addShell(opponentPTray.get(7 - i).steal());
                }

                if (updateTray(i, currentPTray)) {
                    break;
                }
            }
            Log.d("checks", _currentShells + "");


            while (_currentShells > 0) {

                Log.d("checks", _currentShells + "");

                Log.d("checks", "inside while loop");
                //Check for turn repeats and distrbutes to head of current player
                if (_currentShells == 1) {
                    current.setIsTurn(true);
                    opponent.setIsTurn(false);

                }
                if (_currentShells != 0) {
                    if (updateTray(7, currentPTray)) {
                        break;
                    }

                }

                Log.d("checks", _currentShells + "");

                //if shells remains, distrbute among enemy trays except head
                if (_currentShells != 0) {
                    //Other player's tray changes
                    for (int i = 0; i < 7; ++i) {
                        if (updateTray(i, opponentPTray)) break;
                    }
                }

                Log.d("checks", _currentShells + "");
                //if shell remains, distribute among current player trays except head
                if (_currentShells != 0) {
                    for (int i = 0; i < 7; ++i) {
                        if (updateTray(i, currentPTray)) {
                            break;
                        }
                    }
                }
            }
        }

        public void firstTime() {
            Log.d("check", "entering fristTime");
            current.setIsTurn(false);
            opponent.setIsTurn(true);

            Tray clicked = current.getSelectedTray();
            Tray opTray = opponent.getSelectedTray();
            _opponentShells = opTray.getShells();
            opTray.removeShell();
            clicked.removeShell();

            //current players side first
            for (int i = currentPTray.indexOf(clicked) + 1; i < 7; ++i) {
                if (_currentShells == 1 && currentPTray.get(i).getShells() == 0) {
                    currentPTray.get(i).addShell(opponentPTray.get(7 - i).steal());
                }

                if (updateTray(i, currentPTray)) {
                    break;
                }
            }

            //oppents side first
            for (int i = opponentPTray.indexOf(opTray) + 1; i < 7; ++i) {
                if (_opponentShells == 1 && opponentPTray.get(i).getShells() == 0) {
                    opponentPTray.get(i).addShell(currentPTray.get(7 - i).steal());
                }

                if (updateTrayp2(i, opponentPTray)) {
                    break;
                }
            }
            //bonus rounds to be calbrated
            if (_currentShells == 1 || _opponentShells == 1) {
                if (_currentShells == 1 || _opponentShells == 1) {
                    firstTime = true;
                    current.setCounter(0);
                    opponent.setCounter(0);
                } else if (_currentShells == 1) {
                    current.setBonusRound(true);
                    current.setIsTurn(true);
                    opponent.setIsTurn(false);

                } else {
                    opponent.setBonusRound(true);
                    current.setIsTurn(false);
                    opponent.setIsTurn(true);
                }

            }
            //added to heads
            if (_currentShells != 0) {
                updateTray(7, currentPTray);
            }
            if (_opponentShells != 0) {
                updateTrayp2(7, opponentPTray);
            }


            //if shells remains, distrbute among enemy trays except head
            if (_currentShells != 0) {
                //Other player's tray changes
                for (int i = 0; i < 7; ++i) {
                    if (updateTray(i, opponentPTray)) break;
                }
            }

            if (_opponentShells != 0) {
                //Other player's tray changes
                for (int i = 0; i < 7; ++i) {
                    if (updateTrayp2(i, currentPTray)) break;
                }
            }

        }


        @Override
        public void run() {
            Log.d("checks", "entered run class");
            Tray clicked = current.getSelectedTray();
            current.set_shellsholding(clicked.getShells());

            currentPTray = current.getTraySet();
            opponentPTray = opponent.getTraySet();

            //get shells from current tray
            _currentShells = current.get_shellsholding();


            Log.d("checks", _currentShells + "");
            if (firstTime) {
                current.setSelectedTray(clicked);
                current.addCounter(1);
                if (current.getCounter() > 0 && opponent.getCounter() > 0) {

                    firstTime=false;
                    firstTime();
                } }
            else if (current.getBonusRound()) {

                    current.setIsTurn(false);
                    opponent.setIsTurn(true);
                    normalUpdate();
                }
            else {
                    current.setIsTurn(false);
                    opponent.setIsTurn(true);
                    normalUpdate();
                }
            }


    }
}



