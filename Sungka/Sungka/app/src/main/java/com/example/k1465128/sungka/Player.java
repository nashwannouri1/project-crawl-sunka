package com.firstapp.nashwan.finalsungka;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by nashwan on 10/21/2015.
 */
public class Player {

    private String name;
    private ArrayList<Tray> traySet;
    private ArrayList<TextView> playerTVSet;
    private boolean canRepeatTurn, isTurn, turnComplete, bonusRound;
    private boolean canMakeTurn;
    private Tray selectedTray;
    private int counter,_shellsholding;


    public Player() {
        name = "steve";
        traySet = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            traySet.add(new Tray());
        }
        Tray head = new Tray();
        head.setValue(0);
        traySet.add(head);
        playerTVSet = new ArrayList<>();
        canRepeatTurn = false;
        isTurn = true;
        turnComplete = false;
        canMakeTurn = false;
        bonusRound=false;
        selectedTray = new Tray();
        _shellsholding=0;
        counter=0;
    }

    public int get_shellsholding() {
        return _shellsholding;
    }

    public void set_shellsholding(int n) {
        _shellsholding = n;
    }


    public int getCounter() {
        return counter;
    }

    public void setCounter(int n) {
        counter = n;
    }


    public void addCounter(int n) {
        counter = counter + n;
    }

    public void setSelectedTray(Tray tray) {
        selectedTray = tray;
    }

    public Tray getSelectedTray() {
        return selectedTray;
    }



    public void setIsTurn(boolean b) {
        isTurn = b;
    }

    /**
     * get the ArrayList of Views
     *
     * @return ArrayList of Views
     */


    /**
     * get the Tray ArrayList
     *
     * @return the Tray ArrayList
     */
    public ArrayList<Tray> getTraySet() {
        return traySet;
    }

    /**
     * set the TexyView Array
     *
     * @param textViewArray
     */
    public void setTextViewArray(ArrayList<TextView> textViewArray) {
        playerTVSet = textViewArray;
    }



    public void setCanMakeTurn(boolean t) {
        canMakeTurn = t;
    }

    /**
     * checks to see if its a valid move;
     *
     * @param v view to be checked
     * @return if the move is valid;
     */
    public boolean ViewBelongsToPlayer(View v) {

        TextView ts = (TextView) v;
        Log.d("checks", "before the if"+ playerTVSet.contains(ts) +isTurn );
        if (playerTVSet.contains(ts) && isTurn) {
            Log.d("checks", "after if");
            selectedTray = traySet.get(playerTVSet.indexOf(ts));
            if (selectedTray.getShells() != 0 && playerTVSet.indexOf(ts)!=7) {
                return true;
            }
        }
        return false;
    }
    public boolean getBonusRound(){
        return bonusRound ;
    }
    public void  setBonusRound(boolean b){ bonusRound=b;}


}
