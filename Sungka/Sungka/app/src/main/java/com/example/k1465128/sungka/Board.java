package com.firstapp.nashwan.finalsungka;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by nashwan on 10/21/2015.
 */
public class Board extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.board);

        //references for player1 TextView in xml
        _playerOneViewSet.add((TextView) findViewById(R.id.p1t0));
        _playerOneViewSet.add((TextView) findViewById(R.id.p1t1));
        _playerOneViewSet.add((TextView) findViewById(R.id.p1t2));
        _playerOneViewSet.add((TextView) findViewById(R.id.p1t3));
        _playerOneViewSet.add((TextView) findViewById(R.id.p1t4));
        _playerOneViewSet.add((TextView) findViewById(R.id.p1t5));
        _playerOneViewSet.add((TextView) findViewById(R.id.p1t6));
        _playerOneViewSet.add((TextView) findViewById(R.id.p1t7));


        //references for player1 TextView in xml
        _playerTwoViewSet.add((TextView) findViewById(R.id.p2t0));
        _playerTwoViewSet.add((TextView) findViewById(R.id.p2t1));
        _playerTwoViewSet.add((TextView) findViewById(R.id.p2t2));
        _playerTwoViewSet.add((TextView) findViewById(R.id.p2t3));
        _playerTwoViewSet.add((TextView) findViewById(R.id.p2t4));
        _playerTwoViewSet.add((TextView) findViewById(R.id.p2t5));
        _playerTwoViewSet.add((TextView) findViewById(R.id.p2t6));
        _playerTwoViewSet.add((TextView) findViewById(R.id.p2t7));

        //references and chooses the firstPlayer
        _displayPlayer = (TextView) findViewById(R.id.CurPl);
    }

    private ArrayList<TextView> _playerOneViewSet, _playerTwoViewSet;   //players TextViews
    private ArrayList<Tray> _playerOneTray, _playerTwoTray;
    private TextView _displayPlayer;
    private Player player1, player2;
    private Game game;

    public Board()
    {
        player1=  new Player();
        player2 = new Player();
        _playerOneViewSet = new ArrayList<>();
        _playerTwoViewSet = new ArrayList<>();
        _playerOneTray = player1.getTraySet();
        _playerTwoTray = player2.getTraySet();
        player1.setTextViewArray(_playerOneViewSet);
        player2.setTextViewArray(_playerTwoViewSet);
        game= new Game(player1,player2);
    }

    public void move(View v)
    {


        game.runMove(v);
        for(int i=0;i<8;++i){
            _playerOneViewSet.get(i).setText(_playerOneTray.get(i).getShells()+"-");
            _playerTwoViewSet.get(i).setText(_playerTwoTray.get(i).getShells()+"-");

        }
    }




}
