package com.firstapp.nashwan.finalsungka;

/**
 * Created by nashwan on 10/21/2015.
 */
public class Tray {
    private int shells;

    @Override
    public String toString() {
        return "Tray{" +
                "shells=" + shells +
                '}';
    }

    public Tray() {
        shells = 7;
    }


    public void setValue(int n) {
        shells = n;
    }

    public int getShells() {
        return shells;
    }

    public void removeShell() {
        shells=0;
    }

    public void addShell(int n){
        shells += n;

    }

    public int steal(){
        int s = shells;
        shells = 0;
        return s;
    }
}

